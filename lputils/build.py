#!/usr/bin/python3

import subprocess
import random
import os
import grp
import pwd
import lputils.shared


class Build:
    def __init__(self):
        print("Initializing... Please wait...")
        self.groups = ["ingen", "staff", "visitors", "rnd"]
        self.users = ["jthorne", "imalcolm", "sharding", "jhammond", "rtembo", "asidhu", "lmurphy", "ecarr", "rburke"]
        self.items = []
        self.file_names = ["ParkTaxes20", "LifeInsuranceClaim087", "RaptorDNASequencing", "FutureDNASequencing", "Park_WelcomeVideo", "JeepFleet", "ExplorerFleet", "Headlight", "Flareorders", "ContainmentRepairs", "FleetMaintenance", "EggIncubationStatus", "OctFuelOrders", "FutureDigCords", "NovCafeteriaSupply", "Gift_Sales2019", "Fencegrids", "FlightsNov", "HardwareOrders", "NedryDisciplinaryReport", "InvestorReport12", "PhoneDir", "InGenSecurityRoster"]
        self.folder_names = ["RnD", "Staff Docs", "Park Plans", "Dig Sites", "Sales", "HR", "Insurances", "Contracts", "IT", "Administration", "Vehicle Ops", "Visitor Logs", "Archives", "Cargo Manifests", "Gov Docs", "Visitor Presentations", "Fuel Orders", "Debug"]
#        self.check_ran()
        self.create_symlinks()
        self.compile_programs()
        self.build_groups()
        self.build_users()
        self.build_folder_list()
        self.build_file_list()
        self.write_state()
        lputils.reset.Reset()


#    @staticmethod
#    def check_ran():
#        if os.path.isfile('/etc/LP/state.json'):
#            print(
#                'This file is only meant to be run once.\n' +
#                'If you wish to create a new set of files/folders, exit this docker container and run it again'
#            )
#            exit()

    @staticmethod
    def create_symlinks():
        for cmd in ['q', 'c', 'r']:
            os.symlink('/opt/LP/LinuxPermissions/LinuxPermissions.py', f'/usr/local/bin/{cmd}')

    @staticmethod
    def compile_programs():
        d = '/opt/LP/LinuxPermissions/res/'
        for f in os.listdir(d):
            if f.endswith('.c'):
                f = d+f
                subprocess.call(['gcc', '-o', f.replace('.c',''), f])

    def build_groups(self):
        for group in self.groups:
            subprocess.call(["addgroup", group])

    def build_users(self):
        for user in self.users:
            subprocess.call(["adduser", "-D", user])
            with open(f"/etc/sudoers.d/{user}", "w+") as f:
                f.write(f"{user} ALL=(ALL) NOPASSWD: ALL")
            while random.randint(0, 1):
                subprocess.call(["adduser", user, random.choice(self.groups)])

    def build_folder_list(self):
        # Temporarily add "/LP/random/"
        self.items.append(self.define_item("/LP/random/"))

        for x in range(random.randint(99, 100)):
            while True:
                folder = random.choice(self.items)['name']
                folder += random.choice(self.folder_names) + "/"
                if folder.count("/") <= 4 and folder not in self.items:
                    break

            item = self.define_item(folder)
            self.items.append(item)

        # Remove "/LP/random/"
        self.items = self.items[1:]

    def build_file_list(self):
        files = []
        for x in range(random.randint(len(self.items), len(self.items) + 50)):
            dest = random.choice(self.items)['name'] + random.choice(self.file_names)
            if random.randint(0, 1):
                item = self.define_item(dest, "docx")
            else:
                item = self.define_item(dest, "pdf")
            files.append(item)
        self.items += files

    def define_item(self, name, item_type="folder"):
        if item_type != "folder":
            name += "." + item_type

        return {
            "name": name,
            "uid": pwd.getpwnam(random.choice(self.users)).pw_uid,
            "gid": grp.getgrnam(random.choice(self.groups + self.users)).gr_gid,
            "perm": int("".join([str(random.randint(0, 7)) for x in range(4)])),
            "type": item_type
        }

    def write_state(self):
        lputils.shared.write_state(self.users, self.groups, self.items)
