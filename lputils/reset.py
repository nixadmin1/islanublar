#!/usr/bin/python3

import os
import shutil
import lputils.shared


class Reset:
    def __init__(self):
        self.state = lputils.shared.read_state()
        print("Setting /LP/random to default state...")
        
        self.recreate_all()
        self.set_permissions()

    def recreate_all(self):
        shutil.rmtree("/LP/random/",ignore_errors=True)
        os.makedirs("/LP/random", exist_ok=True)
        for i in self.state['items']:
            lputils.shared.create_item(i)

    def set_permissions(self, item = None):
        if item is None:
            os.umask(0)
            item = self.state['items']

        if isinstance(item, list):
            for i in item:
                self.set_permissions(i)
        else:
            if isinstance(item, dict):
                # Permission must be set first, otherwise it may be overwritten
                os.chmod(item['name'], item['perm'])
                os.chown(item['name'], item['uid'], item['gid'])
