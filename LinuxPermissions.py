#!/usr/bin/python3

import argparse
import os
import pty
from lputils import *

class LinuxPermissions:
    def __init__(self):
        self.parse_args()

    def parse_args(self):
        parser = argparse.ArgumentParser()
        group = parser.add_mutually_exclusive_group()
        group.add_argument('-b', '--build', help="Builds default state for /LP/random", action="store_true")
        group.add_argument('-r', '--reset', help="Reset permissions", action="store_true")
        args = parser.parse_args()
        if args.reset:
            if os.geteuid() == 0:
                reset.Reset()
            else:
                print("Please run this command as root.\nExample:\n\t$ sudo r\n\t# r")
        elif args.build:
            build.Build()
            os.system('clear')
            self.print_welcome()
            pty.spawn("/bin/bash")
        else:
            parser.print_help()

    @staticmethod
    def print_welcome():
        print("===========================================================================")
        print("Welcome!\n")
        print("The following commands are available for assistance:")
        print("\t- r: Restore Default State\n\n")
        print("You are expected to answer questions related to the randomly generated users & permissions on CTFD.'")
        print("===========================================================================")

if __name__ == "__main__":
    LinuxPermissions()

